# jun/07/2022 15:14:41 by RouterOS 7.2.3
# software id = 6X00-AQAV
#
# model = RBD52G-5HacD2HnD
# serial number = E5780F4E3920
/interface bridge
add name=bstrm protocol-mode=none
add admin-mac=DC:2C:6E:0D:6D:16 auto-mac=no comment=defconf name=lan
/interface ethernet
set [ find default-name=ether1 ] mtu=1516
/interface pppoe-client
add allow=chap,mschap1,mschap2 disabled=no interface=bstrm keepalive-timeout=30 max-mru=1500 max-mtu=1500 name=mngt use-peer-dns=yes user=m9999c0@exo.cat
/interface vlan
add interface=ether1 mtu=1516 name=ether1.24 vlan-id=24
/interface list
add comment=defconf name=WAN
add comment=defconf name=LAN
/interface lte apn
set [ find default=yes ] ip-type=ipv4 use-network-apn=no
/interface wireless security-profiles
set [ find default=yes ] group-key-update=1h supplicant-identity=MikroTik
add authentication-types=wpa2-psk eap-methods="" group-key-update=1h management-protection=allowed mode=dynamic-keys name=wifi_secrets supplicant-identity=""
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-g/n channel-width=20/40mhz-XX country=spain disabled=no distance=indoors frequency=auto installation=indoor mode=ap-bridge \
    security-profile=wifi_secrets ssid=eXO_886D1A_2G station-roaming=enabled wireless-protocol=802.11
set [ find default-name=wlan2 ] band=5ghz-n/ac channel-width=20/40mhz-Ce country=spain disabled=no distance=indoors frequency=auto installation=indoor mode=ap-bridge \
    security-profile=wifi_secrets ssid=eXO_886D1B_5G station-roaming=enabled wireless-protocol=802.11
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp interface=lan name=defconf
/interface pppoe-client
add add-default-route=yes allow=chap,mschap1,mschap2 disabled=no interface=bstrm keepalive-timeout=30 max-mru=1500 max-mtu=1500 name=exo profile=default-encryption use-peer-dns=\
    yes user=i9999c0@exo.cat
/queue simple
add limit-at=150M/150M max-limit=150M/150M name=xob target=exo,exo
/interface bridge filter
add action=set-priority chain=output new-priority=3 out-bridge=bstrm passthrough=yes
/interface bridge port
add bridge=lan comment=defconf ingress-filtering=no interface=ether2
add bridge=lan comment=defconf ingress-filtering=no interface=ether3
add bridge=lan comment=defconf ingress-filtering=no interface=ether4
add bridge=lan comment=defconf ingress-filtering=no interface=ether5
add bridge=lan comment=defconf ingress-filtering=no interface=wlan1
add bridge=lan comment=defconf ingress-filtering=no interface=wlan2
add bridge=bstrm interface=ether1.24
/ip neighbor discovery-settings
set discover-interface-list=LAN
/ip settings
set max-neighbor-entries=8192
/ipv6 settings
set max-neighbor-entries=8192
/interface list member
add comment=defconf interface=lan list=LAN
add interface=exo list=WAN
add interface=mngt list=LAN
/interface ovpn-server server
set auth=sha1,md5
/ip address
add address=192.168.88.1/24 comment=defconf interface=lan network=192.168.88.0
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 comment=defconf name=router.lan
/ip firewall filter
add action=accept chain=input comment="defconf: accept established,related,untracked" connection-state=established,related,untracked
add action=drop chain=input comment="defconf: drop invalid" connection-state=invalid
add action=accept chain=input comment="defconf: accept ICMP" protocol=icmp
add action=accept chain=input comment="defconf: accept to local loopback (for CAPsMAN)" dst-address=127.0.0.1
add action=drop chain=input comment="defconf: drop all not coming from LAN" in-interface-list=!LAN
add action=accept chain=forward comment="defconf: accept in ipsec policy" ipsec-policy=in,ipsec
add action=accept chain=forward comment="defconf: accept out ipsec policy" ipsec-policy=out,ipsec
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related hw-offload=yes
add action=accept chain=forward comment="defconf: accept established,related, untracked" connection-state=established,related,untracked
add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
add action=drop chain=forward comment="defconf: drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new in-interface-list=WAN
/ip firewall nat
add action=masquerade chain=srcnat comment="defconf: masquerade" ipsec-policy=out,none out-interface-list=WAN
/ipv6 address
add from-pool=exo-prefix interface=lan
/ipv6 dhcp-client
add add-default-route=yes interface=exo pool-name=exo-prefix pool-prefix-length=56 request=address,prefix use-peer-dns=no
/ipv6 firewall filter
add action=accept chain=input protocol=icmpv6
add action=accept chain=input dst-port=546 in-interface-list=WAN protocol=udp
add action=accept chain=input connection-state=established,related
add action=drop chain=input
add action=accept chain=forward connection-state=established,related
add action=drop chain=forward connection-state=!established,related in-interface-list=WAN
/ipv6 firewall mangle
add action=change-mss chain=forward new-mss=1420 out-interface-list=WAN protocol=tcp tcp-flags=syn
add action=change-mss chain=forward in-interface-list=WAN new-mss=1420 protocol=tcp tcp-flags=syn
/system clock
set time-zone-name=Europe/Madrid
/system identity
set name=eXO_886D15_hAPac2
/system leds
add leds=user-led type=on
/system routerboard mode-button
set enabled=yes on-event=wifi_onoff
/system script
add dont-require-permissions=no name=wifi_onoff owner=admin policy=read,write source=":log info \"Button pushed\"\
    \n\
    \n:foreach i in=[/interface wireless find] do={\
    \n:local IfaceName [/interface ethernet get \$i name]\
    \n\
    \n:if ([/interface get \$i disabled]) do={\
    \n/interface ethernet set \$i disabled=no\
    \n/system leds set [find where leds=\"user-led\"] type=on\
    \n:log info \"Interface wireless \$IfaceName enabled\"\
    \n} else={\
    \n/interface ethernet set \$i disabled=yes\
    \n/system leds set [find where leds=\"user-led\"] type=off\
    \n:log info \"Interface wireless \$IfaceName disabled\"\
    \n}\
    \n}"
/tool mac-server
set allowed-interface-list=LAN
/tool mac-server mac-winbox
set allowed-interface-list=LAN
/tool sniffer
set file-name=test.pcap filter-interface=exo memory-limit=1000KiB