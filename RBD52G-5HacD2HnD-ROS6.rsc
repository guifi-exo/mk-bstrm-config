# dec/30/2019 08:22:56 by RouterOS 6.46.1
# software id = KH7M-E995
#
# model = RBD52G-5HacD2HnD
# serial number = A6470A4EA865
/interface bridge
add name=bstrm
add admin-mac=74:4D:28:80:C8:29 auto-mac=no comment=defconf name=lan
/interface pppoe-client
add add-default-route=yes disabled=no interface=bstrm keepalive-timeout=30 \
    max-mru=1480 max-mtu=1480 name=exo password=123456789 user=\
    myuser0@exo.cat
add interface=bstrm keepalive-timeout=30 max-mru=1480 max-mtu=1480 name=mngt \
    password=987654321 use-peer-dns=yes user=my_other_user@exo.cat
/interface vlan
add interface=ether1 name=ether1.24 vlan-id=24
/interface list
add comment=defconf name=WAN
add comment=defconf name=LAN
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
add authentication-types=wpa2-psk eap-methods="" management-protection=\
    allowed mode=dynamic-keys name=wifi_secrets supplicant-identity="" \
    wpa2-pre-shared-key=918273645
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    country=spain disabled=no mode=ap-bridge name=wlan3 security-profile=\
    wifi_secrets ssid=eXO_80E6E4_hAPac2
set [ find default-name=wlan2 ] band=5ghz-a/n/ac channel-width=\
    20/40/80mhz-eCee country=spain disabled=no frequency=5200 mode=ap-bridge \
    name=wlan4 security-profile=wifi_secrets ssid=eXO_80E6E4_hAPac2
/ip hotspot profile
set [ find default=yes ] html-directory=flash/hotspot
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=lan name=defconf
/interface bridge filter
add action=set-priority chain=output new-priority=3 out-bridge=bstrm \
    passthrough=yes
/interface bridge port
add bridge=lan comment=defconf interface=ether2
add bridge=lan comment=defconf interface=ether3
add bridge=lan comment=defconf interface=ether4
add bridge=lan comment=defconf interface=ether5
add bridge=lan comment=defconf interface=wlan3
add bridge=lan comment=defconf interface=wlan4
add bridge=bstrm interface=ether1.24
/ip neighbor discovery-settings
set discover-interface-list=LAN
/interface list member
add comment=defconf interface=lan list=LAN
add interface=exo list=WAN
add interface=mngt list=LAN
/ip address
add address=192.168.88.1/24 comment=defconf interface=lan network=\
    192.168.88.0
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router.lan
/ip firewall filter
add action=accept chain=input comment=\
    "defconf: accept established,related,untracked" connection-state=\
    established,related,untracked
add action=drop chain=input comment="defconf: drop invalid" connection-state=\
    invalid
add action=accept chain=input comment="defconf: accept ICMP" protocol=icmp
add action=drop chain=input comment="defconf: drop all not coming from LAN" \
    in-interface-list=!LAN
add action=accept chain=forward comment="defconf: accept in ipsec policy" \
    ipsec-policy=in,ipsec
add action=accept chain=forward comment="defconf: accept out ipsec policy" \
    ipsec-policy=out,ipsec
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add action=accept chain=forward comment=\
    "defconf: accept established,related, untracked" connection-state=\
    established,related,untracked
add action=drop chain=forward comment="defconf: drop invalid" \
    connection-state=invalid
add action=drop chain=forward comment=\
    "defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat \
    connection-state=new in-interface-list=WAN
/ip firewall nat
add action=masquerade chain=srcnat comment="defconf: masquerade" \
    ipsec-policy=out,none out-interface-list=WAN
/ip ssh
set allow-none-crypto=yes forwarding-enabled=remote
/ipv6 address
# address pool error: pool not found: exo-prefix (4)
add eui-64=yes from-pool=exo-prefix interface=lan
/ipv6 dhcp-client
add add-default-route=yes interface=exo pool-name=exo-prefix \
    pool-prefix-length=56 request=prefix use-peer-dns=no
/ipv6 firewall filter
add action=accept chain=input protocol=icmpv6
add action=accept chain=input dst-port=546 in-interface-list=WAN protocol=udp
add action=accept chain=input connection-state=established,related
add action=drop chain=input
add action=accept chain=forward connection-state=established,related
add action=drop chain=forward connection-state=!established,related \
    in-interface-list=WAN
/ipv6 nd
set [ find default=yes ] advertise-dns=no interface=lan
/system clock
set time-zone-name=Europe/Madrid
/system identity
set name=eXO_80E6E4_hAPac2
/system leds
add leds=user-led type=on
/system routerboard mode-button
set enabled=yes on-event=wifi_onoff
/system script
add dont-require-permissions=no name=wifi_onoff owner=admin policy=read,write \
    source=":log info \"Button pushed\"\
    \n\
    \n:foreach i in=[/interface wireless find] do={\
    \n:local IfaceName [/interface ethernet get \$i name]\
    \n\
    \n:if ([/interface get \$i disabled]) do={\
    \n/interface ethernet set \$i disabled=no\
    \n/system leds set [find where leds=\"user-led\"] type=on\
    \n:log info \"Interface wireless \$IfaceName enabled\"\
    \n} else={\
    \n/interface ethernet set \$i disabled=yes\
    \n/system leds set [find where leds=\"user-led\"] type=off\
    \n:log info \"Interface wireless \$IfaceName disabled\"\
    \n}\
    \n}"
/tool mac-server
set allowed-interface-list=LAN
/tool mac-server mac-winbox
set allowed-interface-list=LAN
